const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const db = require('./db');
const helpers = require('./helpers');
const bcrypt = require('bcryptjs');


passport.use('local.signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {

    await db.query('SELECT * FROM usuario WHERE username = ?;', [username], function (err, rows, fields) {
        if (err) { throw err; }
        else {
            if (rows.length > 0) {
                const user = rows[0];
                /*const validPass = await helpers.matchPassword(password, user.password)
                if(validPass){
                    done(null, user, req.flash('bienvenido', 'Bienvenido(a) '+user.nombre+' '+user.apellido));
                } else {
                    done(null, false, req.flash('msError', 'La contraseña es incorrecta'));
                }*/
                bcrypt.compare(password, user.password, function (err, isMatch) {
                    if (err) {
                        throw err;
                    } else if (isMatch) {
                        if(user.estado === 1){
                            done(null, user);
                        } else if(user.estado === 0) {
                            done(null, false, req.flash('error', 'El acceso a su cuenta a sido deshabilitado.'));
                        } else {
                            done(null, false, req.flash('error', 'A ocurrido un error inesperado.'));
                        }
                    } else {
                        done(null, false, req.flash('error', 'Usuario o contraseña incorrectos'));
                    }
                });
            } else {
                return done(null, false, req.flash('error', 'El usuario no existe'));
                //return done(null, false);
            }
        }
    });

}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    await db.query('SELECT * FROM usuario WHERE id=?;', [id], function (err, rows, fields) {
        done(null, rows[0]);
    });
});